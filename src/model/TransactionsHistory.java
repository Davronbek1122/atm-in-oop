package model;

public class TransactionsHistory {
    private int id;
    private String fromCard;
    private String toCard;
    private double summ;
    private double bank_deposit;
    private int atmId;
    private String action;

    public TransactionsHistory() {
    }

    public TransactionsHistory(int id, String fromCard, String toCard, double summ, double bank_deposit, int atmId, String action) {
        this.id = id;
        this.fromCard = fromCard;
        this.toCard = toCard;
        this.summ = summ;
        this.bank_deposit = bank_deposit;
        this.atmId = atmId;
        this.action=action;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFromCard() {
        return fromCard;
    }

    public void setFromCard(String fromCard) {
        this.fromCard = fromCard;
    }

    public String getToCard() {
        return toCard;
    }

    public void setToCard(String toCard) {
        this.toCard = toCard;
    }

    public double getSumm() {
        return summ;
    }

    public void setSumm(double summ) {
        this.summ = summ;
    }

    public double getBank_deposit() {
        return bank_deposit;
    }

    public void setBank_deposit(double bank_deposit) {
        this.bank_deposit = bank_deposit;
    }

    public int getAtmId() {
        return atmId;
    }

    public void setAtmId(int atmId) {
        this.atmId = atmId;
    }

    @Override
    public String toString() {
        return "TransactionsHistory{" +
                "id=" + id +
                ", fromCard='" + fromCard + '\'' +
                ", toCard='" + toCard + '\'' +
                ", summ=" + summ +
                ", bank_deposit=" + bank_deposit +
                ", atmId=" + atmId +
                '}';
    }
}
