package model;

public class CardTypes {
    private int id;
    private String cardType;

    public CardTypes(int id, String cardType) {
        this.id = id;
        this.cardType = cardType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    @Override
    public String toString() {
        return  (id+1) +". "+
                cardType;
    }
}
