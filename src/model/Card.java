package model;

public class Card {
    private int id;
    private String bank_card_type;
    private String card_number;
    private String password;
    private String bank_name;
    private String expire_date;
    private double balance;
    private Boolean active;



    public Card(int id, String bank_card_type, String card_number, String password, String bank_name, String expire_date, double balance, Boolean active) {
        this.id = id;
        this.bank_card_type = bank_card_type;
        this.card_number = card_number;
        this.password = password;
        this.bank_name = bank_name;
        this.expire_date = expire_date;
        this.balance = balance;
        this.active = active;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBank_card_type() {
        return bank_card_type;
    }

    public void setBank_card_type(String bank_card_type) {
        this.bank_card_type = bank_card_type;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }



    @Override
    public String toString() {
        return id+1 +".  "+
                bank_name+
                ",  " +bank_card_type+
                ", card number: " + card_number+
//                ", password='" + password + '\'' +
                ", expire date: " + expire_date ;
    }
}
