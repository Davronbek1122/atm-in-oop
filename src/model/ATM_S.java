package model;

public class ATM_S {
    private int id;
    private String BankName;
    private double atm_profit;
    private double atm_Bank_balance;
    private double commissionPercent;


    public ATM_S(int id, String bankName, double atm_profit, double atm_Bank_balance, double commissionPercent) {
        this.id = id;
        BankName = bankName;
        this.atm_profit = atm_profit;
        this.atm_Bank_balance = atm_Bank_balance;
        this.commissionPercent = commissionPercent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public double getAtm_profit() {
        return atm_profit;
    }

    public void setAtm_profit(double atm_profit) {
        this.atm_profit = atm_profit;
    }

    public double getAtm_Bank_balance() {
        return atm_Bank_balance;
    }

    public void setAtm_Bank_balance(double atm_Bank_balance) {
        this.atm_Bank_balance = atm_Bank_balance;
    }

    public double getCommissionPercent() {
        return commissionPercent;
    }

    public void setCommissionPercent(double commissionPercent) {
        this.commissionPercent = commissionPercent;
    }

    @Override
    public String toString() {
        return "ATM_S{" +
                "id=" + id +
                ", BankName='" + BankName + '\'' +
                ", atm_profit=" + atm_profit +
                ", atm_Bank_balance=" + atm_Bank_balance +
                ", commissionPercent=" + commissionPercent +
                '}';
    }
}
