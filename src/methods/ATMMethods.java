package methods;

import dataBase.DB;
import model.ATM_S;
import model.Transaction;
import model.TransactionsHistory;
import model.User;

import java.util.Scanner;

public class ATMMethods {

    public static void addNewATM() {
        Scanner scanner = new Scanner(System.in);
        String bankName = "";
        while (true) {
            System.out.print("Enter ATM bankName with location: ");
            boolean bool = true;
            bankName = scanner.nextLine();
            for (int i = 0; i < DB.atmS.size(); i++) {
                if (bankName.equals(DB.atmS.get(i).getBankName())) {
                    bool = false;
                    break;
                }
            }
            if (!bool)
                System.out.println(Methods.GFG.RED_ITALIC + " This bank name is already used " + Methods.GFG.BANANA_YELLOW);
            else break;
        }
        System.out.print("Enter cash balance in this ATM : ");
        double atm_balance = 0;
        while (true) {
            String balance= scanner.next();
            atm_balance = CheckToDouble.checkToDouble(balance);
            try {
                if (atm_balance >= 0) {
                    break;
                } else if (atm_balance == -1) {
                    System.out.print(Methods.GFG.RED_ITALIC+"Please correct balance -> " + Methods.GFG.BANANA_YELLOW);
                } else {
                    System.out.print(Methods.GFG.RED_ITALIC + "Balance can not - " + Methods.GFG.BANANA_YELLOW);
                    System.out.print("Please enter correct balance -> ");
                }
            } catch (Exception e) {
                System.out.println(Methods.GFG.RED_ITALIC + "Error command!" + Methods.GFG.BANANA_YELLOW);
            }
        }
        System.out.print("Enter ATM comission in percent -> ");
        double comission = 0;
        while (true) {
            String balance= scanner.next();
            comission = CheckToDouble.checkToDouble(balance);
            try {
                if (comission > 0) {
                    DB.atmS.add(new ATM_S(DB.atmS.size(), bankName, Transaction.MIN_LIMIT_OF_CARD, atm_balance, comission));
                    System.out.println(Methods.GFG.CYAN_ITALIC + " Successfully added new ATM " + Methods.GFG.BANANA_YELLOW);
                    return;
                } else
                    System.out.print(Methods.GFG.RED_ITALIC + "Please enter correct comission  -> " + Methods.GFG.BANANA_YELLOW);
            } catch (Exception e) {
                System.out.print(Methods.GFG.RED_ITALIC + "Please enter correct comission -> " + Methods.GFG.BANANA_YELLOW);
            }
        }


    }

    public static void showAllTRansactionAfterChoosenAtm(ATM_S atmS) {
        // all money transfers
        System.out.println(Methods.GFG.CYAN_ITALIC + "\n All money transfers in \"" + atmS.getBankName() + "\" ATM " + Methods.GFG.BANANA_YELLOW);
        boolean bool = true;
        for (TransactionsHistory history : DB.transactionsHistories) {
            if (history.getAtmId() == atmS.getId() && history.getAction().equals("transfer") ) {
                System.out.println("from -> \"" + history.getFromCard() + "\" card  to \"" + history.getToCard() + "\",  summ-> " + history.getSumm() + "so'm," + " ATM get " + (history.getSumm() * history.getBank_deposit() / 100) + " so'm" + " total withdraw payment -> " + ((history.getSumm()) + ((history.getSumm() * history.getBank_deposit() / 100))));
                bool = false;
            }
        }

        boolean bool1 = true;
        for (TransactionsHistory history : DB.transactionsHistories) {
            if (history.getAtmId() == atmS.getId() && history.getAction().equals("transfer") ) {
                System.out.println("from -> \"" + history.getFromCard() + "\" card  to \"" + history.getToCard() + "\",  summ-> " + history.getSumm() + "so'm," + " ATM get " + (history.getSumm() * history.getBank_deposit() / 100) + " so'm" + " total withdraw payment -> " + ((history.getSumm()) + ((history.getSumm() * history.getBank_deposit() / 100))));
                bool1 = false;
            }
        }
        if (bool1 && bool)
            System.out.println(Methods.GFG.RED_ITALIC + "Don't have yet any transaction" + Methods.GFG.BANANA_YELLOW);


        // fill card transactions
        System.out.println(Methods.GFG.CYAN_ITALIC + " All fill Card balance" + Methods.GFG.BANANA_YELLOW);

        boolean boolfill = true;
        for (TransactionsHistory history : DB.transactionsHistories) {
            if (history.getAtmId() == atmS.getId() && history.getAction().equals("fill") ) {
                System.out.println("for -> \"" + history.getToCard() + "\",  summ-> " + history.getSumm() + "so'm" + ", card filled -> " + (history.getSumm() - (history.getSumm() * history.getBank_deposit() / 100)) + " ATM get " + (history.getSumm() * history.getBank_deposit() / 100) +
                        " so'm");
                boolfill = false;
            }
        }
        if (boolfill)
            System.out.println(Methods.GFG.RED_ITALIC + "Don't have yet any \"fill card balance\" action!" + Methods.GFG.BANANA_YELLOW);

        //  withdraw money
        System.out.println(Methods.GFG.CYAN_ITALIC + " All withdraw from card balance" + Methods.GFG.BANANA_YELLOW);
        boolean boolwithdraw = true;
        for (TransactionsHistory history : DB.transactionsHistories) {
            if (history.getAtmId() == atmS.getId() && history.getAction().equals("withdraw") ) {
                System.out.println("from -> \"" + history.getFromCard() + "\",  summ-> " + history.getSumm() + "so'm,  " + atmS.getBankName() + "  ATM get " + (history.getSumm() * history.getBank_deposit() / 100) + " so'm" + " total withdraw payment -> " + ((history.getSumm()) + ((history.getSumm() * history.getBank_deposit() / 100))));
                boolwithdraw = false;
            }
        }
        if (boolwithdraw)
            System.out.println(Methods.GFG.RED_ITALIC + "Don't have yet any \"withdraw money\" action!" + Methods.GFG.BANANA_YELLOW);
    }

    public static void showAllATMForAdmin() {
        System.out.println();
        for (int i = 0; i < DB.atmS.size(); i++) {
            System.out.println((i + 1) + ". " + DB.atmS.get(i).getBankName() + " comission is -> " + DB.atmS.get(i).getCommissionPercent() + " %");
        }
    }

        public static void deleteATM(ATM_S atm, User user) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(Methods.GFG.RED_ITALIC + " Are you sure to delete this -> \"" + atm.getBankName() + "\" ATM ? " + Methods.GFG.BANANA_YELLOW);
        System.out.println("1. Yes  0. Back  ->  ");
        String str = "";
        while (true) {
            str = scanner.next();
            if (str.equals("0")) return;
            else if (str.equals("1")) break;
            else
                System.out.println(Methods.GFG.RED_ITALIC + "Please enter correct action !" + Methods.GFG.BANANA_YELLOW);
        }
        System.out.print("Please enter your password: ");
        String password = scanner.next();
        if (password.equals(user.getPassword())) {
            DB.atmS.remove(atm);
            System.out.println(Methods.GFG.CYAN_ITALIC + " You seccessfully remove this -> \"" + atm.getBankName() + "\"" + Methods.GFG.BANANA_YELLOW);
        }
    }

    //  new
    public static void updateATM(User user, ATM_S atm) {
        Scanner numscann = new Scanner(System.in);
        Scanner Stringscann = new Scanner(System.in);
        while (true) {
            System.out.println("1. Bank name: " + atm.getBankName() + "\t" + "2. ATM balance: " + atm.getAtm_Bank_balance() + "\t " + " 3. Atm commission: " + atm.getCommissionPercent() + "%");
            System.out.println();

            System.out.println("1-Change Bank name                2-Fill ATM cash balance" + "\n" +
                    "3-Get cash from ATM                4-Change ATM's commission percent" + "\n" +
                    "0-Back");


            System.out.print("Enter your command here: ");
            String choice = Stringscann.next();

            switch (choice) {
                case "0":
                    AdminConsole.baseMenuForAdmin(user);
                    return;
                case "1":
                    changeBankName(user, atm);
                    break;
                case "2":
                    fill_ATM_cash_balance(user, atm);
                    break;
                case "3":
                    getCashFromATM(user, atm);
                    break;
                case "4":
                    changeComission(user, atm);
                    break;
                default:
                    System.out.println("Wrong command please enter command again...");
            }

        }
    }


    public static void changeComission(User user, ATM_S atm_s) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter password -> ");
        String password = scanner.nextLine();
        if (user.getPassword().equals(password)) {
            while (true) {
                String change = "";
                System.out.println("Change comission percent -> ");
                change = scanner.next();
                double comission = CheckToDouble.checkToDouble(change);
                try {
                    if (comission >= 0) {
                        atm_s.setCommissionPercent(comission);
                        System.out.println(Methods.GFG.CYAN_ITALIC + "Successfully change comission percent" + Methods.GFG.BANANA_YELLOW);
                        updateATM(user, atm_s);
                    } else if (comission == -1) {
                        System.out.println("Error entered");
                    } else {
                        System.out.println("Error entered");
                    }
                } catch (Exception e) {
                    System.out.println("Error entered");
                }

            }

        }
        System.out.println("Error password");
    }


    public static void changeBankName(User user, ATM_S atm_s) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter password -> ");
        String password = scanner.nextLine();
        if (user.getPassword().equals(password)) {
            String changeBankName = "";
            while (true) {
                System.out.print("Change bank name -> ");
                changeBankName = scanner.nextLine();
                if (!changeBankName.isEmpty()) {
                    atm_s.setBankName(changeBankName);
                    System.out.println("Succesfully changed");
                    return;
                } else {
                    System.out.println("Error");
                }

            }
        }
        System.out.println("Error password");
    }


    public static void getCashFromATM(User user, ATM_S atm_s) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter password -> ");
        String password = scanner.nextLine();
        if (user.getPassword().equals(password)) {
            double getCashFromATM = 0;
            double remainCash = 0;
            while (true) {
                System.out.println("Current balance ->" + atm_s.getAtm_Bank_balance());
                System.out.print("Enter amount  -> ");
                String get=scanner.next();
                getCashFromATM = CheckToDouble.checkToDouble(get);
                try {
                    if (atm_s.getAtm_Bank_balance() == 0) {
                        System.out.println("Not enough cash \uD83D\uDE45\u200D♂️");
                    }
                    if (getCashFromATM > 0 && getCashFromATM <= atm_s.getAtm_Bank_balance()) {
                        remainCash = atm_s.getAtm_Bank_balance() - getCashFromATM;
                        atm_s.setAtm_Bank_balance(remainCash);
                        System.out.println("Operation successfully completed " + Methods.GFG.GREEN_ITALIC + " \uD83D\uDCB8" + Methods.GFG.BANANA_YELLOW);
                        return;
                    } else if (getCashFromATM > atm_s.getAtm_Bank_balance()) {
                        System.out.println("Not enough cash \uD83D\uDE45\u200D♂️");
                        return;
                    } else if (getCashFromATM<0) {
                        System.out.println(Methods.GFG.RED_ITALIC+"Please enter correct summ!"+ Methods.GFG.BANANA_YELLOW);
                    }
                } catch (Exception e) {
                    System.out.println("Error");
                }
            }
        }
    }


    public static void fill_ATM_cash_balance(User user, ATM_S atm_s) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter password -> ");
        String password = scanner.nextLine();
        if (user.getPassword().equals(password)) {
            String deposit1 = "";
            double deposit=0;
            double summ = 0;
            while (true) {
                System.out.println("Atm balance: " + atm_s.getAtm_Bank_balance());
                System.out.print("Enter amount of deposit: ");
                deposit1 = scanner.next();
                deposit=CheckToDouble.checkToDouble(deposit1);
                try {
                    if (deposit > 0) {
                        summ = atm_s.getAtm_Bank_balance() + deposit;
                        atm_s.setAtm_Bank_balance(summ);
                        System.out.println("Operation successfully completed");
                        return;
                    } else {
                        System.out.println(Methods.GFG.RED_ITALIC+"Error deposit"+ Methods.GFG.BANANA_YELLOW);
                    }
                } catch (Exception e) {
                    System.out.println("Error");
                }
            }
        }
    }



    public static void showAllAtmProfit(User user){
        double allSumm=0;
        for (int i = 0; i < DB.atmS.size(); i++) {
            ATM_S atm_s=DB.atmS.get(i);
            System.out.println(atm_s.getBankName()+" ATM, all profit -> "+atm_s.getAtm_profit()+" summ");
            allSumm+= atm_s.getAtm_profit();
        }
        System.out.println("   All ATMs' profit -> "+allSumm );
    }

}
