package methods;

public class CheckToDouble {

    public static double checkToDouble(String str){
        str=str.replaceAll(",",".");// 1,2,3 => 1.2.3weee
        int counter=0;
        boolean bool=true;
        for (int i = 0; i < str.length(); i++) {
            if ( Character.isLetter(str.charAt(i))){
                return -1;
            }else if (str.charAt(i)=='.') counter++;
        }
        for (int i = 0; i < str.length(); i++) {
             if (Character.isDigit(str.charAt(i))){
                bool=false;
                break;
            }
        }
        if (bool) return -1;
        if (counter>1) return -1;
        return Double.parseDouble(str);
    }

    public static String checkToNumber(String str){
        for (int i = 0; i < str.length(); i++) {
            if (Character.isLetter(str.charAt(i))) return "";
        }
        return str;
    }
}
