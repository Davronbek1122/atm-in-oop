package methods;

import dataBase.DB;
import model.*;

import java.util.Scanner;
import java.util.function.Function;

public class CardMethods {


    public static void blockOrActivateCard(){
        Scanner scanner=new Scanner(System.in);
        if (!showBankCards()){
            System.out.println("Please choose card : ");
            while (true){
                int choosenCard= scanner.nextInt();
                try {
                    if (choosenCard>0 && choosenCard<= DB.cards.size()){
                        Card card=DB.cards.get(choosenCard-1);
                        if (card.getActive()){
                            while (true){
                                System.out.println(Methods.GFG.RED_ITALIC+card.getCard_number()+" ->  card is now Active! ");
                                System.out.print("Do you want to block? :  1. Yes  0. Back"+ Methods.GFG.BANANA_YELLOW);
                                String str=scanner.next();
                                if (str.equals("0")){
                                    return;
                                } else if (str.equals("1")) {
                                    break;
                                }else System.out.println(Methods.GFG.RED_ITALIC+"Error command bro !"+ Methods.GFG.BANANA_YELLOW);
                            }
                        }else {
                            while (true){
                                System.out.println(Methods.GFG.RED_ITALIC+card.getCard_number()+" ->  card is now Blocked ! ");
                                System.out.print("Do you want to activate ? :  1. Yes  0. Back -> "+ Methods.GFG.BANANA_YELLOW);
                                String str=scanner.next();
                                if (str.equals("0")){
                                    return;
                                } else if (str.equals("1")) {
                                    break;
                                }else System.out.println(Methods.GFG.RED_ITALIC+"Error command bro !"+ Methods.GFG.BANANA_YELLOW);
                            }
                        }
                        DB.cards.get(choosenCard-1).setActive(!DB.cards.get(choosenCard-1).getActive());

                        System.out.print(Methods.GFG.CYAN_ITALIC+" You ");
                        if (DB.cards.get(choosenCard-1).getActive()) System.out.print(" activate this card -> ");
                        else System.out.print(" blocked this card ->");
                        System.out.println(DB.cards.get(choosenCard-1).getCard_number()+ Methods.GFG.BANANA_YELLOW);
                        return;
                    }else System.out.println(Methods.GFG.RED_ITALIC+"Please choose correct card"+ Methods.GFG.BANANA_YELLOW);
                }catch (Exception e){
                    System.out.println(Methods.GFG.RED_ITALIC+"Please choose correct card"+ Methods.GFG.BANANA_YELLOW);
                }
            }
        }

    }

    public static boolean showBankCards(){
        boolean yes=true;
        for (int i = 0; i < DB.cards.size(); i++) {
            yes=false;
            System.out.print(DB.cards.get(i));
            if ((DB.cards.get(i).getActive())) System.out.print(", Active \n");
            else System.out.print(", Blocked \n");
        }
        if (yes) System.out.println(Methods.GFG.CYAN_ITALIC+" Not have any Bank Card yet!"+ Methods.GFG.BANANA_YELLOW);
        return yes;
    }

    public static void addNewBankCard() {
        Scanner scanner = new Scanner(System.in);
        Scanner scannerString = new Scanner(System.in);
        String bankCardType =showCardTypes().getCardType();
        String cardNumber = "";
        while (true) {
            System.out.print("Enter card number -> ");
            String str=scanner.next();
            cardNumber = CheckToDouble.checkToNumber(str);
            boolean bool = true;
            if (cardNumber.length() == 4) {
                for (int i = 0; i < DB.cards.size(); i++) {
                    if (cardNumber.equals(DB.cards.get(i).getCard_number())) {
                        bool = false;
                    }
                }
                if (!bool) {
                    System.out.println("This bank card already added");
                } else {
                        break;
                }
            } else if (cardNumber.length() < 4) {
                System.out.println("doesn't enough number for card number");
            } else if (cardNumber.length() > 4) {
                System.out.println("the number of numbers on the card is more than 16");
            }

        }

        System.out.print("Enter pin-code -> ");
        String password = "";
        while (true) {
            String pass=scanner.next();
            password =CheckToDouble.checkToNumber(pass);
            try {
                if (password.length() == 4) {
                    break;
                } else {
                    System.out.println("Entered error pin-code");
                }

            } catch (Exception e) {
                System.out.println("Entered error pin-code");

            }
        }
        String bankName = "";
        while (true) {
            System.out.print("Enter Bank name -> ");
            bankName = scannerString.nextLine();
            if (!bankName.isEmpty()) {
                break;
            } else {
                System.out.println("Error entered bank name");
            }
        }
        String expireTime = "";
        while (true) {
            System.out.print("Enter expire time (11/22) -> ");
            expireTime = scannerString.nextLine();
            if (expireTime.length() == 5) {
                break;
            } else {
                System.out.println("Error entered expire time");
            }
        }

        DB.cards.add(new Card(DB.cards.size(), bankCardType, cardNumber, password, bankName, expireTime, Transaction.MIN_LIMIT_OF_CARD, true));
        System.out.println("Successfully added new Card");

    }

    public static CardTypes showCardTypes(){
        Scanner scanner=new Scanner(System.in);
        for (CardTypes cardType : DB.cardTypes) {
            System.out.println(cardType);
        }
        System.out.print("Please choose Card Type -> ");
        int coosenCardType=0;
        while (true){
            String str=scanner.next();
            coosenCardType=(int) CheckToDouble.checkToDouble(str);
            if (coosenCardType>0 && coosenCardType<=DB.cardTypes.size()){
                return DB.cardTypes.get(coosenCardType-1);
            }else System.out.print(Methods.GFG.RED_ITALIC+"Please choose correct Card Type ->"+ Methods.GFG.BANANA_YELLOW);
        }
    }
}
