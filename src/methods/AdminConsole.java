package methods;

import dataBase.DB;
import model.ATM_S;
import model.Card;
import model.TransactionsHistory;
import model.User;
import java.util.Scanner;

import static methods.ATMMethods.showAllATMForAdmin;
import static methods.AdminMethods.addNewUSer;
import static methods.AdminMethods.showAllAdmin;

public class AdminConsole {

    public static void baseMenuForAdmin(User user) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nHello \"" + user.getUsername() + "\" ");
        while (true) {
//            System.out.println(Methods.GFG.CYAN_ITALIC + "\nNow you are in -> " + atm.getBankName() + Methods.GFG.BANANA_YELLOW);
            System.out.println(Methods.GFG.BANANA_YELLOW + "\nPlease choose: ");
            System.out.println("1. Show all transaction in ATMs'         2. Activate or block bankCards\n" +
                    "3. Show all BankCards                    4. Add new BankCard  \n" +
                    "5. Update ATM                            6. Show all ATM\n" +
                    "7. Delete ATM                            8. Add new ATM\n" +
                    "9. Show ATM cash balance                10. show ATM comission profit\n" +
                    "11. Add new Admin                       12. Show all Admin\n" +
                    "13. Change my password                  14. Show all ATMs' profit \n" +
                    Methods.GFG.RED_ITALIC + "                  0 -> EXIT" + Methods.GFG.BANANA_YELLOW);
            String choosen_menu = scanner.next();
            switch (choosen_menu) {
                case "0":
                    return;
                case "1":
                    ATMMethods.showAllTRansactionAfterChoosenAtm(Methods.showAllATM());
                    break;
                case "2":
                    CardMethods.blockOrActivateCard();
                    break;
                case "3":
                    CardMethods.showBankCards();
                    break;
                case "4":
                    CardMethods.addNewBankCard();
                    break;
                case "5":
                    ATMMethods.updateATM(user, Methods.showAllATM());
                    break;
                case "6":
                    showAllATMForAdmin();
                    break;
                case "7":
                    ATMMethods.deleteATM(Methods.showAllATM(), user);
                    break;
                case "8":
                    ATMMethods.addNewATM();
                    break;
                case "9":
                    ATM_S atm = Methods.showAllATM();
                    System.out.println(" balance of-> " + atm.getBankName() + " is " + atm.getAtm_Bank_balance() + " soum");
                    break;
                case "10":
                    ATM_S atm2 = Methods.showAllATM();
                    System.out.println(" comission profit of-> " + atm2.getBankName() + " is " + atm2.getAtm_profit() + " soum");
                    break;
                case "11":
                    addNewUSer();
                    break;
                case "12":
                    showAllAdmin(user);
//                    AdminMethods.showAllAdmin(user);
                    break;
                case "13":
                    AdminMethods.changeAdminPassword(user);
                    break;
                case "14":
                    ATMMethods.showAllAtmProfit(user);
                    break;
                default:
                    System.out.println(Methods.GFG.RED_ITALIC + " Error command!" + Methods.GFG.BANANA_YELLOW);
            }
        }
    }

}
