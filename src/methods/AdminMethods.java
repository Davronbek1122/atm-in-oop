package methods;

import dataBase.DB;
import model.User;

import java.util.Scanner;

public class AdminMethods {
    public static void addNewUSer() {
        Scanner scanner = new Scanner(System.in);
        Scanner scanner1 = new Scanner(System.in);
        System.out.print("Enter Name -> ");
        String name = scanner.nextLine();
        System.out.print("Enter surName -> ");
        String surname=scanner1.nextLine();
        for (int i = 0; i < DB.users.size(); i++) {
            if (name.equals(DB.users.get(i).getName()) && surname.equals(DB.users.get(i).getSurName())){
                System.out.println(Methods.GFG.RED_ITALIC+"This person is already added!"+ Methods.GFG.BANANA_YELLOW);
                return;
            }
        }
        System.out.print("Enter userName -> ");
        String user_name = scanner.nextLine();

        String password;
        boolean pass=true;
        while (true){
            System.out.print("Enter password -> ");
            password=scanner1.nextLine();
            for (int i = 0; i < DB.users.size(); i++) {
                if (password.equals(DB.users.get(i).getPassword())){
                    pass=false;
                    System.out.println(Methods.GFG.RED_ITALIC+"This password is already used! "+ Methods.GFG.BANANA_YELLOW);
                    break;
                }
            }
            if (pass) break;
        }
        DB.users.add(new User(DB.users.size(), name, surname, user_name, password, 0));
        System.out.println(Methods.GFG.CYAN_ITALIC+"Successfully added!"+ Methods.GFG.BANANA_YELLOW);
    }
    public static void showAllAdmin(User user){
        int counter=1;
        boolean bool=true;
        for (int i = 0; i < DB.users.size(); i++) {
            User user1=DB.users.get(i);
            if (user1.getId()!= user.getId() && DB.users.get(i).getRole() == 0) {
                bool=false;
                System.out.println(counter+". "+user1.getName()+"  "+user1.getSurName());
                counter++;
            }
        }
        if (bool) System.out.println(Methods.GFG.CYAN_ITALIC+"No other users !"+ Methods.GFG.BANANA_YELLOW);
    }

    public static void changeMyPassword(User user) {
        Scanner scanner = new Scanner(System.in);
        String lastPasswordCheck = "";
        while (true) {
            System.out.println("\"0\" -> back or enter your last password: ");
            lastPasswordCheck = scanner.nextLine();
            if (lastPasswordCheck.equals("0")) return;
            if (user.getPassword().equals(lastPasswordCheck)) {
                break;
            } else
                System.out.println(Methods.GFG.RED_ITALIC + " Error password! Please try again!" + Methods.GFG.BANANA_YELLOW);
        }
        while (true) {
            System.out.println("Enter your new password : " + Methods.GFG.RED_ITALIC + "Note: Enter more than 4 characters" + Methods.GFG.BANANA_YELLOW);
            String newPassword = scanner.nextLine();
            if (newPassword.length() >= 4) {
                System.out.println("Please Confirm your new Password");
                String newPasswordConfirm = scanner.nextLine();
                if (newPassword.equals(newPasswordConfirm)) {
                    user.setPassword(newPassword);
                    System.out.println("Succesfully changed your password!");
                    break;
                }
            } else {
                System.out.println("Please Enter more than 4 characters !");
            }
        }
    }
    public static void changeAdminPassword(User user){
        Scanner scanner=new Scanner(System.in);
        System.out.print("Enter \"0\" to back or Please enter old password -> ");
        String oldPassword= "";
        while (true){
            oldPassword= scanner.next();
            if (oldPassword.equals("0")) return;
            if (user.getPassword().equals(oldPassword)){
                break;
            }else System.out.print(Methods.GFG.RED_ITALIC+"Wrong password"+
                    Methods.GFG.BANANA_YELLOW +"\nPlease enter again or enter \"0\" to back-> ");
        }
        System.out.print("Please enter new password -> ");
        String newPassword= scanner.next();
        System.out.print("Please re-enter new password -> ");
        String newPassword2= "";
        while (true){
            newPassword2= scanner.next();
            if (newPassword2.equals("0")) return;
            if (newPassword.equals(newPassword2)){
                break;
            }else System.out.print(Methods.GFG.RED_ITALIC+"Wrong re-password"+
                    Methods.GFG.BANANA_YELLOW +"\nPlease enter again or enter \"0\" to back -> ");
        }
        user.setPassword(newPassword);
        System.out.println(Methods.GFG.CYAN_ITALIC+" Your password successfully added!"+ Methods.GFG.BANANA_YELLOW);
    }

}
