package methods;

import dataBase.DB;

public class CheckToInt {

    public static int checkToInt(String str){
        String res="";
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))){
               res+=str.charAt(i);
            }
        }
        if (res.length()> DB.transactionsHistories.size()+1) return -1;
        if (res.isEmpty()) return -1;
        return Integer.parseInt(res);
    }
}
