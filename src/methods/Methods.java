package methods;
import dataBase.DB;
import model.ATM_S;
import model.Card;
import model.User;

import java.util.Scanner;

public class Methods {

    public class GFG {

        // Declaring ANSI_RESET so that we can reset the color
        public static final String ANSI_RESET = "\033[38;2;240;238;113m";
        public static final String BLACK = "\033[0;30m";   // BLACK
        public static final String RED = "\033[0;31m";     // RED
        public static final String GREEN = "\033[0;32m";   // GREEN
        public static final String YELLOW = "\033[0;33m";  // YELLOW
        public static final String BLUE = "\033[0;34m";    // BLUE
        public static final String PURPLE = "\033[0;35m";  // PURPLE
        public static final String CYAN = "\033[0;36m";    // CYAN
        public static final String WHITE = "\033[0;37m";   // WHITE
        public static final String LIGHT_PURPLE = "\033[38;2;200;0;200m"; //LIGHT PURPLE
        public static final String TEAL = "\033[38;2;0;225;221m"; //TEAL
        public static final String ORANGE = "\033[38;2;225;153;0m"; //ORANGE
        public static final String LIGHT_GREEN = "\033[38;2;136;255;0m"; //LIGHT GREEN
        public static final String LIGHT_BLUE = "\033[38;2;120;172;255m"; //LIGHT BLUE
        public static final String DARK_BLUE = "\033[38;2;72;0;255m"; //DARK BLUE
        public static final String ROSY_PINK = "\033[38;2;255;0;162m"; //ROSY PINK
        public static final String BROWN = "\033[38;2;135;82;62m"; //BROWN
        public static final String FOREST_GREEN = "\033[38;2;62;135;81m"; //FOREST GREEN
        public static final String BANANA_YELLOW = "\u001B[0m" ; //BANANA YELLOW
        public static final String DARK_RED = "\033[38;2;145;40;16m"; //DARK RED
        public static final String LIGHT_PINK = "\033[38;2;255;153;240m"; //LIGHT PINK

        // StrikeOut
        public static final String BLACK_STRIKE = "\033[9;30m";   // BLACK
        public static final String RED_STRIKE = "\033[9;31m";     // RED
        public static final String GREEN_STRIKE = "\033[9;32m";   // GREEN
        public static final String YELLOW_STRIKE = "\033[9;33m";  // YELLOW
        public static final String BLUE_STRIKE = "\033[9;34m";    // BLUE
        public static final String PURPLE_STRIKE = "\033[9;35m";  // PURPLE
        public static final String CYAN_STRIKE = "\033[9;36m";    // CYAN
        public static final String WHITE_STRIKE = "\033[9;37m";   // WHITE

        //Italic
        public static final String BLACK_ITALIC = "\033[3;30m";   // BLACK
        public static final String RED_ITALIC = "\033[3;31m";     // RED
        public static final String GREEN_ITALIC = "\033[3;32m";   // GREEN
        public static final String YELLOW_ITALIC = "\033[3;33m";  // YELLOW
        public static final String BLUE_ITALIC = "\033[3;34m";    // BLUE
        public static final String PURPLE_ITALIC = "\033[3;35m";  // PURPLE
        public static final String CYAN_ITALIC =  "\033[3;36m";    // CYAN
        public static final String WHITE_ITALIC = "\033[3;37m";   // WHITE

        // Bold
        public static final String BLACK_BOLD = "\033[1;30m";  // BLACK
        public static final String RED_BOLD = "\033[1;31m";    // RED
        public static final String GREEN_BOLD = "\033[1;32m";  // GREEN
        public static final String YELLOW_BOLD = "\033[1;33m"; // YELLOW
        public static final String BLUE_BOLD = "\033[1;34m";   // BLUE
        public static final String PURPLE_BOLD = "\033[1;35m"; // PURPLE
        public static final String CYAN_BOLD = "\033[1;36m";   // CYAN
        public static final String WHITE_BOLD = "\033[1;37m";  // WHITE

        // Underline
        public static final String BLACK_UNDERLINED = "\033[4;30m";  // BLACK
        public static final String RED_UNDERLINED = "\033[4;31m";    // RED
        public static final String GREEN_UNDERLINED = "\033[4;32m";  // GREEN
        public static final String YELLOW_UNDERLINED = "\033[4;33m"; // YELLOW
        public static final String BLUE_UNDERLINED = "\033[4;34m";   // BLUE
        public static final String PURPLE_UNDERLINED = "\033[4;35m"; // PURPLE
        public static final String CYAN_UNDERLINED = "\033[4;36m";   // CYAN
        public static final String WHITE_UNDERLINED = "\033[4;37m";  // WHITE

        // Background
        public static final String BLACK_BACKGROUND = "\033[40m";  // BLACK
        public static final String RED_BACKGROUND = "\033[41m";    // RED
        public static final String GREEN_BACKGROUND = "\033[42m";  // GREEN
        public static final String YELLOW_BACKGROUND = "\033[43m"; // YELLOW
        public static final String BLUE_BACKGROUND = "\033[44m";   // BLUE
        public static final String PURPLE_BACKGROUND = "\033[45m"; // PURPLE
        public static final String CYAN_BACKGROUND = "\033[46m";   // CYAN
        public static final String WHITE_BACKGROUND = "\033[47m";  // WHITE
        public static final String LIGHT_PURPLE_BACKGROUND = "\033[48;2;200;0;200m"; //LIGHT PURPLE
        public static final String TEAL_BACKGROUND = "\033[48;2;0;225;221m"; //TEAL
        public static final String ORANGE_BACKGROUND = "\033[48;2;225;153;0m"; //ORANGE
        public static final String LIGHT_GREEN_BACKGROUND = "\033[48;2;136;255;0m"; //LIGHT GREEN
        public static final String LIGHT_BLUE_BACKGROUND = "\033[48;2;120;172;255m"; //LIGHT BLUE
        public static final String DARK_BLUE_BACKGROUND = "\033[48;2;72;0;255m"; //DARK BLUE
        public static final String ROSY_PINK_BACKGROUND = "\033[48;2;255;0;162m"; //ROSY PINK
        public static final String BROWN_BACKGROUND = "\033[48;2;135;82;62m"; //BROWN
        public static final String FOREST_GREEN_BACKGROUND = "\033[48;2;62;135;81m"; //FOREST GREEN
        public static final String BANANA_YELLOW_BACKGROUND = "\033[48;2;240;238;113m"; //BANANA YELLOW
        public static final String DARK_RED_BACKGROUND = "\033[48;2;145;40;16m"; //DARK RED
        public static final String LIGHT_PINK_BACKGROUND = "\033[48;2;255;153;240m"; //LIGHT PINK

        // High Intensity
        public static final String BLACK_BRIGHT = "\033[0;90m";  // BLACK
        public static final String RED_BRIGHT = "\033[0;91m";    // RED
        public static final String GREEN_BRIGHT = "\033[0;92m";  // GREEN
        public static final String YELLOW_BRIGHT = "\033[0;93m"; // YELLOW
        public static final String BLUE_BRIGHT = "\033[0;94m";   // BLUE
        public static final String PURPLE_BRIGHT = "\033[0;95m"; // PURPLE
        public static final String CYAN_BRIGHT = "\033[0;96m";   // CYAN
        public static final String WHITE_BRIGHT = "\033[0;97m";  // WHITE

        // Bold High Intensity
        public static final String BLACK_BOLD_BRIGHT = "\033[1;90m"; // BLACK
        public static final String RED_BOLD_BRIGHT = "\033[1;91m";   // RED
        public static final String GREEN_BOLD_BRIGHT = "\033[1;92m"; // GREEN
        public static final String YELLOW_BOLD_BRIGHT = "\033[1;93m";// YELLOW
        public static final String BLUE_BOLD_BRIGHT = "\033[1;94m";  // BLUE
        public static final String PURPLE_BOLD_BRIGHT = "\033[1;95m";// PURPLE
        public static final String CYAN_BOLD_BRIGHT = "\033[1;96m";  // CYAN
        public static final String WHITE_BOLD_BRIGHT = "\033[1;97m"; // WHITE

    }
    public static int showBaseMenu(){
        Scanner scanner=new Scanner(System.in);
       while (true){
           System.out.println(GFG.BANANA_YELLOW+" Hello \n " +
                   "Please choose: 1. LogIn  0. Exit");
           String choosen_menu= scanner.next();
           switch (choosen_menu){
               case "1":
                   return 1;
               case "0":
                   return 0;
               default:
                   System.out.println("Error command! \nPlease try again!");
           }
       }
    }
    public static int checkPassword(){
        Scanner scanner=new Scanner(System.in);
        int counter=5;
        while (true){
            boolean role=true;
            boolean roole=true;
            System.out.println("After -> "+GFG.RED_ITALIC+(counter-2)+GFG.BANANA_YELLOW+" failed you will be blocked! ");
            System.out.print("Please enter your username or card number : ");
            String username= scanner.nextLine();
            System.out.print("Please enter your password or card pin-code: ");
            String password= scanner.nextLine();
            User user=null;

            if (roole){
                for (int i = 0; i < DB.users.size(); i++) {
                    user=DB.users.get(i);
                    if (user.getUsername().equals(username) && !user.getPassword().equals(password)){
                        role=false;
                        System.out.println(GFG.RED_ITALIC+" Wrong filled, please try again!"+GFG.BANANA_YELLOW);
                    } else if (!user.getUsername().equals(username) && user.getPassword().equals(password)) {
                        role=false;
                        System.out.println(GFG.RED_ITALIC+" Wrong filled, please try again!"+GFG.BANANA_YELLOW);
                    }else if (user.getPassword().equals(password)
                            && user.getUsername().equals(username)){
                        role=false;
                        AdminConsole.baseMenuForAdmin(user);
                        counter=6;
                        break;
                    }
                }
            }
            Card card=null;

            if (role){
                for (int i = 0; i < DB.cards.size(); i++) {
                    card=DB.cards.get(i);
                    if (card.getCard_number().equals(username) && !card.getPassword().equals(password)){
                        roole=false;
                        if (!card.getActive()){
                            System.out.println(GFG.RED_BOLD+" This card is blocked ! Please call to "+card.getBank_name()+GFG.BANANA_YELLOW);
                            break;
                        }
                        System.out.println(GFG.RED_ITALIC+" Wrong filled, please try again!"+GFG.BANANA_YELLOW);
                    }else if (card.getCard_number().equals(username) && card.getPassword().equals(password)){
                        if (card.getActive()){
                            roole=false;
                            ATM_S atm_s=showAllATM();
                            UserMethods.mainMenu(card,atm_s);
                            counter=6;
                            break;
                        }else System.out.println(GFG.RED_BOLD+" This card is blocked ! Please call to "+card.getBank_name()+GFG.BANANA_YELLOW);

                    }else if (!card.getCard_number().equals(username) && card.getPassword().equals(password)){
                        System.out.println(GFG.RED_ITALIC+" Wrong filled, please try again!"+GFG.BANANA_YELLOW);
                        roole=false;
                    }
                }
            }
            counter--;

            if (counter==2){
                System.out.println(GFG.RED_ITALIC+"You are blocked Please try again after some minutes!"+GFG.BANANA_YELLOW);
                if (card!=null){
                    card.setActive(false);
                    return 0;
                }
            }
        }
    }
    public static ATM_S showAllATM(){
        System.out.println();
        Scanner scanner=new Scanner(System.in);
        for (int i = 0; i < DB.atmS.size(); i++) {
            System.out.println((i+1)+". "+DB.atmS.get(i).getBankName()+",  comission is -> "+DB.atmS.get(i).getCommissionPercent()+" %");
        }
        System.out.print("\nPlease choose ATM: ");
        int choosen_atm=0;
        while (true){
            String str=scanner.next();
            choosen_atm=CheckToInt.checkToInt(str);
            try {
                if (choosen_atm>0 && choosen_atm<=DB.atmS.size()){
                    System.out.println();
                    return DB.atmS.get(choosen_atm-1);
                }else  System.out.print(GFG.RED_ITALIC+"Please choose correct ATM -> "+GFG.BANANA_YELLOW);
            }catch (Exception e){
                System.out.print(GFG.RED_ITALIC+"Please choose correct ATM -> "+GFG.BANANA_YELLOW);
            }
        }
    }
    public static Card showAllCard(Card card){
        Scanner scanner=new Scanner(System.in);
        System.out.println();
        for (int i = 0; i < DB.cards.size(); i++) {
            if (!card.getCard_number().equals(DB.cards.get(i).getCard_number())) System.out.println((i+1)+". "+DB.cards.get(i).getCard_number());
            else System.out.println((i+1)+". "+DB.cards.get(i).getCard_number()+GFG.CYAN_ITALIC+" now you are in this Card!"+GFG.BANANA_YELLOW);
        }
        int choosen_atm=0;
        while (true){
            System.out.print("Please choose Card: ");
            String str=scanner.nextLine();
            choosen_atm=(int) CheckToDouble.checkToDouble(str);
            try {
                if (choosen_atm - 1 == card.getId()) {
                    System.out.println(GFG.RED_ITALIC+" You can't choose your Card! "+GFG.BANANA_YELLOW);
                }else if (choosen_atm>0 && choosen_atm<=DB.cards.size()){
                    return DB.cards.get(choosen_atm-1);
                }else System.out.println(GFG.RED_ITALIC+"Please choose correct card!"+GFG.BANANA_YELLOW);
            }catch (Exception e){
                System.out.println(GFG.RED_ITALIC+"Please choose correct card!"+GFG.BANANA_YELLOW);
            }
        }
    }

}
