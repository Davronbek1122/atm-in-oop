package methods;

import dataBase.DB;
import model.*;

import java.util.Scanner;

public class UserMethods {

    public static void mainMenu(Card card, ATM_S atm_s) {
        Scanner strcanner = new Scanner(System.in);
        while (true) {
            System.out.println(Methods.GFG.PURPLE_ITALIC + "\n                       MAIN MENU          " + "\n" +
                    Methods.GFG.CYAN_ITALIC + "    Now you are in -> Bank name -> " + card.getBank_name() + ", card number -> " + card.getCard_number() + " , your balance ->  " + card.getBalance() + "\n" + Methods.GFG.BANANA_YELLOW +
                    "0.Exit                          1.Fill bank card balance " + "\n" +
                    "2.Withdraw                      3.Transfer to another bank card" + "\n" +
                    "4.Transaction history           5.Change bank card password" + "\n");

            System.out.print("Choose your command here: ");
            strcanner = new Scanner(System.in);
            String command = strcanner.nextLine();

            switch (command) {
                case "1":
                    fillBankCardBalance(atm_s, card);
                    break;
                case "2":
                    withdraw(atm_s, card);
                    break;
                case "3":
                    Card tocard = Methods.showAllCard(card);
                    transferToAnotherBankCard(atm_s, card, tocard);
                    break;
                case "4":
                    transactionHistoryForUser(card, atm_s);
                    break;
                case "5":
                    changeBankCardPassword(card);
                    break;
                case "0":
                    return;
                default:
                    System.out.println("Wrong command. Please choose correct command given above!");
            }

        }

    }

    public static void fillBankCardBalance(ATM_S atm_s, Card card) {
        Scanner scanner = new Scanner(System.in);
        Scanner strscanner = new Scanner(System.in);

        double commissionAmount, amountWithoutCommission;
        System.out.print("Enter amount: ");

        while (true){
            String sum = scanner.next();
            double amount = CheckToDouble.checkToDouble(sum);

            if (amount >= 500 && amount <= Transaction.MAX_LIMIT_OF_TARNSACTION) {
              commissionAmount = amount * atm_s.getCommissionPercent() / 100;     // ushlab qolinadigan kommissiya
              amountWithoutCommission = amount - commissionAmount;           //   chistiy tushadigan summa

              System.out.println("Amount to deposit: " + amount + "\n" +
                      "ATM commission percent: " + atm_s.getCommissionPercent() + " % \n" +
                      "Commission amount: " + commissionAmount + " \n" +
                      "Amount to be deposited after commission : " + amountWithoutCommission);

              while (true) {
                  System.out.println("\n" + "1)OK      0) Cancel (Back to main menu  "+ Methods.GFG.RED+"\uD83D\uDD19)"+ Methods.GFG.BANANA_YELLOW);
                  String command = strscanner.next();
                  if (command.equals("1")) {
                      card.setBalance(card.getBalance() + amountWithoutCommission);
                      atm_s.setAtm_profit(atm_s.getAtm_profit() + commissionAmount);
                      DB.transactionsHistories.add(new TransactionsHistory(DB.transactionsHistories.size(), "", card.getCard_number(), amount, atm_s.getCommissionPercent(), atm_s.getId(), "fill"));
                      System.out.println("Operation successfully done "+ Methods.GFG.GREEN+"✅"+ Methods.GFG.BANANA_YELLOW);
                      return;
                  } else if (command.equals("0")) {
                      return;
                  } else {
                      System.out.println("Please choose correct command and try again "+ Methods.GFG.RED+"❌"+ Methods.GFG.BANANA_YELLOW);
                  }
              }
          } else if (amount == -1) {
              System.out.print("Please enter correct amount -> ");
          } else {
              System.out.print("Minimum transaction amount: 500 -> ");
          }
      }
    }


    public static void transactionHistoryForUser(Card card, ATM_S atmS) {

        // all money transfers from this card
        System.out.println(Methods.GFG.CYAN_ITALIC + "\n All money transfers of \"" + card.getCard_number() + "\" in \"" + atmS.getBankName() + "\" ATM " + Methods.GFG.BANANA_YELLOW);
        boolean bool = true;
        for (TransactionsHistory history : DB.transactionsHistories) {
            if (history.getAtmId() == atmS.getId() && history.getAction().equals("transfer") && card.getCard_number().equals(history.getFromCard())) {
                System.out.println("from -> \"" + history.getFromCard() + "\" card  to \"" + history.getToCard() + "\",  summ-> " + history.getSumm() + "so'm," + " ATM get " + (history.getSumm() * history.getBank_deposit() / 100) + " so'm" + " total withdraw payment -> " + ((history.getSumm()) + ((history.getSumm() * history.getBank_deposit() / 100))));
                bool = false;
            }
        }
//        if (bool)
//            System.out.println(Methods.GFG.RED_ITALIC + "Don't have yet any transaction" + Methods.GFG.BANANA_YELLOW);

        // all money transfers to this card
        boolean bool1 = true;
        for (TransactionsHistory history : DB.transactionsHistories) {
            if (history.getAtmId() == atmS.getId() && history.getAction().equals("transfer") && card.getCard_number().equals(history.getToCard())) {
                System.out.println("from -> \"" + history.getFromCard() + "\" card  to \"" + history.getToCard() + "\",  summ-> " + history.getSumm() + "so'm," + " ATM get " + (history.getSumm() * history.getBank_deposit() / 100) + " so'm" + " total withdraw payment -> " + ((history.getSumm()) + ((history.getSumm() * history.getBank_deposit() / 100))));
                bool1 = false;
            }
        }
        if (bool1 && bool)
            System.out.println(Methods.GFG.RED_ITALIC + "Don't have yet any transaction" + Methods.GFG.BANANA_YELLOW);

        // fill card transactions

        System.out.println(Methods.GFG.CYAN_ITALIC + " All fill Card balance actions: " + Methods.GFG.BANANA_YELLOW);
        boolean boolfill = true;
        for (TransactionsHistory history : DB.transactionsHistories) {
            if (history.getAtmId() == atmS.getId() && history.getAction().equals("fill") && card.getCard_number().equals(history.getToCard())) {
                System.out.println("for -> \"" + history.getToCard() + "\",  summ-> " + history.getSumm() + "so'm," + "  card filled -> " + (history.getSumm() - (history.getSumm() * history.getBank_deposit() / 100)) + " ATM get " + (history.getSumm() * history.getBank_deposit() / 100) +
                        " so'm");
                boolfill = false;
            }
        }
        if (boolfill)
            System.out.println(Methods.GFG.RED_ITALIC + "Don't have yet any \"fill card balance\" action!" + Methods.GFG.BANANA_YELLOW);

        //  withdraw money
        System.out.println(Methods.GFG.CYAN_ITALIC + " All withdraw from card balance" + Methods.GFG.BANANA_YELLOW);
        boolean boolwithdraw = true;
        for (TransactionsHistory history : DB.transactionsHistories) {
            if (history.getAtmId() == atmS.getId() && history.getAction().equals("withdraw") && card.getCard_number().equals(history.getFromCard())) {
                System.out.println("from -> \"" + history.getFromCard() + "\",  summ-> " + history.getSumm() + "so'm,  " + atmS.getBankName() + "  ATM get " + (history.getSumm() * history.getBank_deposit() / 100) + " so'm" + " total withdraw payment -> " + ((history.getSumm()) + ((history.getSumm() * history.getBank_deposit() / 100))));
                boolwithdraw = false;
            }
        }
        if (boolwithdraw)
            System.out.println(Methods.GFG.RED_ITALIC + "Don't have yet any \"withdraw money\" action!" + Methods.GFG.BANANA_YELLOW);

    }

    public static void transferToAnotherBankCard(ATM_S atm_s, Card fromcard, Card toCard) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nATM commission percent " + atm_s.getCommissionPercent() + " %");
        while (true) {
            String transfer = "";
            System.out.println("How much do you want transfer -> ");
            transfer = scanner.next();
            double amount = CheckToDouble.checkToDouble(transfer);
            double summ = 0;
            double percent = 0;
            double extraValueofTocard = 0;
            try {
                if (amount > 0) {
                    if (fromcard.getBalance() >= amount + (amount * (atm_s.getCommissionPercent() / 100))) {
                        summ = amount + toCard.getBalance();
                        percent = atm_s.getAtm_profit() + amount * (atm_s.getCommissionPercent() / 100);
                        extraValueofTocard = fromcard.getBalance() - amount - (amount * (atm_s.getCommissionPercent() / 100));
                        toCard.setBalance(summ);
                        atm_s.setAtm_profit(percent);
                        fromcard.setBalance(extraValueofTocard);
                        DB.transactionsHistories.add(new TransactionsHistory(DB.transactionsHistories.size(), fromcard.getCard_number(), toCard.getCard_number(), amount, atm_s.getCommissionPercent(), atm_s.getId(), "transfer"));
                        System.out.println(Methods.GFG.CYAN_ITALIC + "Successfully transfered!" + Methods.GFG.BANANA_YELLOW);
                        return;
                    } else {
                        System.out.println("Don't enough amount!");
                        return;
                    }
                } else {
                    System.out.println("Please enter correct money");
                }
            } catch (Exception e) {
                System.out.println("Error");
            }
        }
    }

    public static void withdraw(ATM_S atm_s, Card fromCard) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("ATM commission percent -> " + atm_s.getCommissionPercent() + " %");
        while (true) {
            String amount = "";
            System.out.print("how much money do you want to withdraw -> ");
            amount = scanner.next();
            double withdraw = CheckToDouble.checkToDouble(amount);
            double summ = 0;
            double percent = 0;
            try {
                if (withdraw > 0) {
                    if (fromCard.getBalance() >= withdraw + (withdraw * (atm_s.getCommissionPercent() / 100))) {
                        if (withdraw<=atm_s.getAtm_Bank_balance()){
                            summ = fromCard.getBalance() - (withdraw + (withdraw * (atm_s.getCommissionPercent() / 100)));
                            percent = atm_s.getAtm_profit() + (withdraw * (atm_s.getCommissionPercent() / 100));
                            fromCard.setBalance(summ);
                            atm_s.setAtm_profit(percent);
                            atm_s.setAtm_Bank_balance(atm_s.getAtm_Bank_balance()-withdraw);
                            DB.transactionsHistories.add(new TransactionsHistory(DB.transactionsHistories.size(), fromCard.getCard_number(), "", withdraw, atm_s.getCommissionPercent(), atm_s.getId(), "withdraw"));
                            System.out.println(Methods.GFG.CYAN_ITALIC + "Successfully withdraw! " + Methods.GFG.BANANA_YELLOW);
                            return;
                        }else {
                            System.out.println(Methods.GFG.RED_ITALIC+" Don't have enough cash balance in ATM! "+ Methods.GFG.BANANA_YELLOW);
                            return;
                        }

                    } else {
                        System.out.println("Don't enough money");
                        return;
                    }
                } else {
                    System.out.println("Please enter correct money");
                }
            } catch (Exception e) {
                System.out.println("Error");
            }

        }
    }

    private static void changeBankCardPassword(Card card) {
        Scanner scanner = new Scanner(System.in);
        Scanner scanner1 = new Scanner(System.in);
        String lastPasswordCheck = "";
        while (true) {
            System.out.println("\"0\" -> back - Or enter your card's pin-code: ");
            lastPasswordCheck = scanner.nextLine();
            if (lastPasswordCheck.equals("0")) return;
            if (card.getPassword().equals(lastPasswordCheck)) {
                break;
            } else
                System.out.println(Methods.GFG.RED_ITALIC + " Error password! Please try again!" + Methods.GFG.BANANA_YELLOW);
        }
        System.out.print("Please enter new pin-code -> ");
        String newPassword ="";
        while (true){
            newPassword = scanner1.next();
            if (checkPasswordTo_4(newPassword)){
                break;
            }else System.out.print(Methods.GFG.RED_ITALIC+" please write only 4 number -> "+ Methods.GFG.BANANA_YELLOW);
        }
        System.out.print(" \"0\" -> back - Or  re-enter new pin-code -> ");
        String newPassword2 = "";
        while (true){
            newPassword2=scanner1.next();
            if (newPassword2.equals("0")) return;
            if (checkPasswordTo_4(newPassword2)) break;
            else System.out.print(" your confirm password is not same with new password, Please try again -> ");
        }
        card.setPassword(newPassword);
        System.out.println(Methods.GFG.CYAN_ITALIC+"Your pin-code successfully changed!"+ Methods.GFG.BANANA_YELLOW);
    }

    public static boolean checkPasswordTo_4(String password) {
        if (password.length() == 4) {
            for (int i = 0; i < password.length(); i++) {
                if (!Character.isDigit(password.charAt(i))){
                    return false;
                }
            }
        }else return false;
        return true;
    }



}
