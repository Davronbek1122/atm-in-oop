import dataBase.DB;
import methods.Methods;
import model.*;

import static methods.Methods.checkPassword;

public class Main {
    public static void main(String[] args) {
        DB.cardTypes.add(new CardTypes(DB.cardTypes.size(), "HUMO"));
        DB.cardTypes.add(new CardTypes(DB.cardTypes.size(), "UzCard"));
        DB.cardTypes.add(new CardTypes(DB.cardTypes.size(), "MasterCard"));
        DB.users.add(new User(DB.users.size(), "Vali","ss","admin", "123",0));
        DB.atmS.add(new ATM_S(DB.atmS.size(), "AnorBank Chorsu", Transaction.MIN_LIMIT_OF_CARD,100_000,1));
        DB.atmS.add(new ATM_S(DB.atmS.size(), "Agrobamk Chorsu", Transaction.MIN_LIMIT_OF_CARD,100_000,1.2));
        DB.cards.add(new Card(DB.cards.size(), "Humo", "1122","1111", "Xalq","11/22", 200_000,true));
        DB.cards.add(new Card(DB.cards.size(), "UzCard", "7777","7777", "Ravnaq","11/29", 300_000,true));

        for (int i = 0; ; i++) {
            switch (Methods.showBaseMenu()){
                case 1:
                   if (checkPassword()==0) return;
                case 0:
                    return;
            }
        }


    }
}
